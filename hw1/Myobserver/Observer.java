package myobserver;

public abstract class Observer {
	protected myobserver.Subject subject;
    public abstract void update();

}
