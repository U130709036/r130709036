package myobserver;

public class ObserverBinary extends Observer {
    public ObserverBinary(Subject subject) {
        this.subject = subject;
        this.subject.add(ObserverBinary.this);
    }

    public void update() {
        System.out.print("Binary : " + Integer.toBinaryString(subject.getState())+"\n");
    }
}