package myobserver;

public class ObserverHexaDecimal extends Observer {
    public ObserverHexaDecimal(Subject subject) {
        this.subject = subject;
        this.subject.add(this);
    }

    public void update() {
        System.out.print("HexaDecimal : " + Integer.toHexString(subject.getState())+"\n");
    }
}