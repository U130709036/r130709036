package myobserver;

import java.util.Scanner;

public class ObserverDemo {
    public static void main( String[] args ) {
        Subject sub = new Subject();
        new ObserverBinary(sub);
        new ObserverDecimal(sub);
        new ObserverHexaDecimal(sub);
        @SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            System.out.print("Enter a number: ");
            sub.setState(scanner.nextInt());
        }
    }
}