package myobserver;

public class ObserverDecimal extends Observer {
    public ObserverDecimal(Subject subject) {
        this.subject = subject;
        this.subject.add( this );
    }

    public void update() {
        System.out.print("Decimal : " + subject.getState(),"\n");
    }
}